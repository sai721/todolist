package com.todo.todo.mapper;

import com.todo.todo.model.Task;
import com.todo.todo.request.TaskRequest;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    Task merge(TaskRequest taskRequest);
    void merge(TaskRequest taskRequest, @MappingTarget Task task);
}