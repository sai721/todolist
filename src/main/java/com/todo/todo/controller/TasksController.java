package com.todo.todo.controller;

import com.todo.todo.exception.TaskException;
import com.todo.todo.request.TaskRequest;
import com.todo.todo.service.TaskService;
import com.todo.todo.model.Task;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController(value = "taskController")
@RequestMapping(path = "/api/task")
@Log4j2
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TasksController {
    private final TaskService taskService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Task> findAll() {
        return this.taskService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Task create(@RequestBody @Validated TaskRequest taskRequest) throws TaskException {
        log.debug("POST /task with request:"+taskRequest);
        Task task = this.taskService.create(taskRequest);
        return task;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Task update(@PathVariable("id") Long id,
                              @RequestBody TaskRequest taskRequest) throws TaskException {
        log.debug("PUT /tasks/"+id+"  with request:"+taskRequest);
        Task task = this.taskService.update(taskRequest, id);
        return task;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Task show(@PathVariable("id") Long id) throws TaskException {
        log.debug("GET /tasks/"+id+"  with id:"+id);
        Task task = taskService.find(id);
        return task;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) throws TaskException {
        log.debug("DELETE /tasks/"+id+"  with id:"+id);
        taskService.delete(id);
    }
}
