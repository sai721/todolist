package com.todo.todo.service;

import com.todo.todo.request.TaskRequest;
import com.todo.todo.exception.TaskException;
import com.todo.todo.mapper.TaskMapper;
import com.todo.todo.model.Task;
import com.todo.todo.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Log4j2
public class TaskService {
    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final SequenceService sequenceService;

    public Task create(TaskRequest taskRequest) throws TaskException {
        if(taskRepository.findByTitle(taskRequest.getTitle())!=null){
            throw new TaskException("Entity already exists with same name");
        }
        Task task = taskMapper.merge(taskRequest);
        task.setId(sequenceService.generateSequence(Task.SEQUENCE_NAME));
        task.setCreatedDate(new Date(System.currentTimeMillis()));
        task.setUpdatedDate(new Date(System.currentTimeMillis()));
        return this.taskRepository.save(task);
    }

    public Task update(TaskRequest taskRequest, Long id) throws TaskException {
        Task task =  find(id);
        taskMapper.merge(taskRequest, task);
        task.setUpdatedDate(new Date(System.currentTimeMillis()));
        return this.taskRepository.save(task);
    }

    public List<Task> findAll(){
        return this.taskRepository.findAll();
    }

    public Task find(Long id) throws TaskException {
        Task task = this.taskRepository.findById(id);
        if(task == null)
            throw new TaskException("Task not found with id"+id);
        return task;
    }

    public void delete(Long id) throws TaskException{
        Task task = find(id);
        this.taskRepository.delete(task);
    }
}
