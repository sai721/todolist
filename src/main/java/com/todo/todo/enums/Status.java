package com.todo.todo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Status {
    TODO(0, "TODO"),
    IN_PROGRESS(1, "IN_PROGRESS"),
    COMPLETED(2, "COMPLETED");

    private Integer index;
    private String name;

    public static Status getStatus(String name){
        switch (name){
            case "TODO":
                return Status.TODO;
            case "IN_PROGRESS":
                return Status.IN_PROGRESS;
            case "COMPLETED":
                return Status.COMPLETED;
        }
        return null;
    }
}
