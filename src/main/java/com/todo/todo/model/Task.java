package com.todo.todo.model;

import com.todo.todo.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@Document(collection = "task")
public class Task {
    @Transient
    public static final String SEQUENCE_NAME = "tasks_sequence";

    @Id
    private Long id;
    private String title;
    private String description;
    private Status status;
    private Date createdDate;
    private Date updatedDate;

}
