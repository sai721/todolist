package com.todo.todo.repository;

import com.todo.todo.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface TaskRepository extends MongoRepository<Task, String> {
    List<Task> findAll();
    Task findById(Long id);

    Task findByTitle(String title);
}