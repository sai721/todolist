package com.todo.todo.request;

import com.todo.todo.enums.Status;
import lombok.Data;

@Data
public class TaskRequest {
    private String title;
    private String description;
    private Status status;
}
